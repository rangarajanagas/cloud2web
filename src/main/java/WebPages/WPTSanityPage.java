package WebPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

public class WPTSanityPage extends WPTBasePage {

	public WPTSanityPage(WebDriver web_driver) {
		super(web_driver);
	}

	/** WELCOME **/
	public By txtusername = By.id("input-username");
	public By txtpassword = By.id("input-password");
	public By btnLogin = By.xpath("//span[text()='Login in']");
	public By chkbxLegal = By.id("checkbox-agree-to-update");
	public By btnContinue = By.xpath("//span[text()='Continue']");

	
	/** HOME PAGE **/
	
	public By lnkMyLibrary = By.xpath("//a[text()='My Library']");
	public By lnkOpen = By.xpath("//*[@id='tabpanel-0']/div/div[2]/div/div/div/div[2]/div/div[1]/div/div[1]/button/span[1]");
	public By btnReadNow = By.xpath("//span[text()='Read Now']");
	public By lnktoc = By.xpath("//button[@title='Table of Content']");
	public By lnkPage = By.xpath("//div[@role='presentation'][1]");
	
	public By chptrSelect = By.xpath("//p[text()='I: The Plant Men']");
	public By icnClose = By.xpath("//div[@class='sc-fHYxKZ dnKCVK']//button[@class='MuiButtonBase-root MuiIconButton-root'][1]");
	
	public By lnkclose = By.xpath("//button[@title='My Library']");
	
	
	public By lnkPreChapter = By.xpath("//button[@title='Previous Chapter']");
	public By lnkNxtChapter = By.xpath("//button[@title='Next Chapter']");
	public By lnkAddBkmark = By.xpath("//button[@title='Add a bookmark']");
	public By lnkBkMarkNotations = By.xpath("//button[@title='Bookmarks & Notations']");
	public By svdBkmark = By.xpath("//span[text()='0% of I: The Plant Men']");
	public By dltBkmark = By.xpath("//button[@title='Delete bookmark']");
	
	/** READER SETTINGS **/
	
	public By lnkReaderSettings = By.xpath("//div[@class='sc-gyUeRy fFXEDN']//button[@type='button'][4]");
	public By lnkLeftAligned = By.xpath("//button[@value='start']");
	public By lnkCenterAligned = By.xpath("//button[@value='justify']");
	public By lnkColorDay = By.xpath("//button[@value='readium-day-on']");
	public By lnkColrSepia = By.xpath("//button[@value='readium-sepia-on']");
	public By lnkColorNight = By.xpath("//button[@value='readium-night-on']");
	
	public By lnkPageScroll = By.xpath("//input[@type='checkbox']");
	public By lnkReset = By.xpath("//span[text()='Reset Settings']");	
	
	
	public By menuAbout = By.className("about-popup");
	public By lblVersion = By.className("appVersion");
	public By spinner = By.className("spinner");
	public By working = By.className("user-action-button");
		
		
	public void selectDropDown(By locator, String value) {
		
		Select optSelect = new Select(web_driver.findElement(locator));
		optSelect.selectByVisibleText(value);
	}
	
	public String getAppVersion() {
		
		clickElement(menuAbout);
		return getElement(lblVersion).getText();
	}


}
