package Helper;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class StartApp {
	
	public WebDriver web_driver;
	public Properties prop;
	Efficacies effic;
	Properties config;
	Runtime runtime;
	
	public String dataSheetName;
	public String databookName;	
	
	public File rootPath;
	public File appPath;
	public File emulPath;
	public File emulAppPath;
	DesiredCapabilities capabilities;
	

	// Starting the driver browser
	 public WebDriver startApp_Web() throws IOException
		{
		  	rootPath = new File(System.getProperty("user.dir"));
			appPath = new File(rootPath, "/src/main/resources/WebResrc/");
			File chromePath = new File(appPath, "chromedriverWPT.exe");
			
			effic = new Efficacies();
			config = effic.loadPropertiesFromResources("environment.properties");
			System.out.println(config);
			
			if (config.getProperty("Browser").equalsIgnoreCase("Chrome")) {
				
				System.setProperty("webdriver.chrome.driver",chromePath.getAbsolutePath());
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--enable-javascript");
				options.setExperimentalOption("useAutomationExtension", false);
				options.setExperimentalOption("excludeSwitches",Collections.singletonList("enable-automation"));
				
				options.addArguments("--disable-extensions");
				options.addArguments("-–disable-notifications");
				options.addArguments("disable-infobars");
				options.addArguments("--start-maximized");
				options.addArguments("--disable-web-security");
				options.addArguments("--no-proxy-server");

				Map<String, Object> prefs = new HashMap<String, Object>();
				prefs.put("credentials_enable_service", false);
				prefs.put("profile.password_manager_enabled", false);

				options.setExperimentalOption("prefs", prefs);
				web_driver = new ChromeDriver(options);
				web_driver.get(config.getProperty("URL"));
				web_driver.manage().window().maximize();
						
			} else if (config.getProperty("Browser").equalsIgnoreCase("IE")) {
				
				System.setProperty("webdriver.ie.driver","");
				web_driver = new InternetExplorerDriver();
		 
				web_driver.get(config.getProperty("ProdURL"));
				web_driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
				web_driver.manage().window().maximize();
			}
			return web_driver;
		}
	
	
	
	
	// Close the Web driver
	public void closeDriver_Web(){
		try {
			web_driver.quit();
			System.out.println("Closing the driver...");
        } catch (Exception e) { }
	}
	

	public void recursiveDelete(File file) {
        //to end the recursive loop
        if (!file.exists())
            return;
        
        //if directory, go inside and call recursively
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                //call recursively
                recursiveDelete(f);
            }
        }
        //call delete to delete files and empty directory
        file.delete();
        System.out.println("Deleted file/folder: "+file.getAbsolutePath());
    }
	
	public void emualtorBatFile() throws IOException
	{
		
		Process process = Runtime.getRuntime().exec("cmd /c Android.bat", null, new File("C:\\Users\\rangarajan\\Documents\\WPT_Automation\\CloudLibrary_DC\\src\\main\\java\\Helper\\"));
		
	}

}
