package WPT;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.Test;

import com.xpandit.testng.annotations.Xray;

import Helper.ExtentTestManager;
import net.sf.json.JSONObject;

public class jSONOutputCls extends WPTSetUp {
	
    @Test
    public void testCase() throws Exception {		
		JSONObject jsonObject = new JSONObject();
	      //Inserting key-value pairs into the json object
	      jsonObject.put("ID", "1");
	      jsonObject.put("First_Name", "CIT");
	      jsonObject.put("Last_Name", "Info");
	      jsonObject.put("Date_Of_Birth", "1981-12-05");
	      jsonObject.put("Place_Of_Birth", "Delhi");
	      jsonObject.put("Country", "India");
	      
//	      try {  	  
//	    	  
//	         FileWriter file = new FileWriter("D:\\CloudLibraryReg\\CloudLibrary_Web\\test-output\\jSONOutput\\output.json");
//	         //file.write(jsonObject.toJSONString());
//	         file.write(jsonObject.toString());
//	         file.close();
//	      } catch (Exception e) {
//	         // TODO Auto-generated catch block
//	         e.printStackTrace();
//	      }
	      
	      try (FileWriter file = new FileWriter("D:\\CloudLibraryReg\\CloudLibrary_Web\\test-output\\jSONOutput\\output.json")) {
	    	  
	            file.write(jsonObject.toString());
	            file.flush();
	 
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	      
	      System.out.println("JSON file created: "+jsonObject);
	   }
        
    }	

