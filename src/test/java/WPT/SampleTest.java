package WPT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;

public class SampleTest extends WPTSetUp {

	@Test(priority=1)
	public void verify_LoginPopup() throws Exception {

		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_LoginPopup");
		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(5);
		web_driver.findElement(sanityPageWeb.chkbxLegal).click();					
		basePageWeb.clickElement(sanityPageWeb.btnContinue);
		
		actualData.add(basePageWeb.getElement(sanityPageWeb.lnkMyLibrary).getText());
		expectedData.add(testData.get("TextMyLibrary"));
		
		Assert.assertEquals(expectedData, actualData);
		
	}
	
	@Test(priority=2)
	public void verify_List() throws Exception {
		
		//String cssSelectorOfSameElements="input[type='submit'][id='button']";
		basePageWeb.waitforPageLoad(5);
		
		basePageWeb.clickElement(sanityPageWeb.lnkMyLibrary);

		basePageWeb.waitforPageLoad(5);
		
		web_driver.findElement(By.xpath("//descendant::span[@class='MuiButton-label'][4]")).click();
		
		//List<WebElement> ele = web_driver.findElements(By.xpath("//div[@id='tabpanel-0']"));
		
		//List<WebElement> ele = web_driver.findElements(By.xpath("//div[@class='MuiGrid-root MuiGrid-container MuiGrid-spacing-xs-3']"));
		
		//List<WebElement> ele = web_driver.findElements(By.xpath("//div[@id='tabpanel-0']//descendant::div[@class='sc-nFpLZ jXWXLS']"));
		
		//for(int i=0; i<ele.size(); i++)
//		{
//			
//			System.out.println("Total no of sugg " + ele.size());
//			System.out.println("Values are " + ele.get(i).getText());
//			
//			if(ele.get(i).getText().contains("Darling Rose Gold"))
//			{
//				basePageWeb.waitforPageLoad(3);
//				//web_driver.findElement(By.xpath("//descendant::span[@class='MuiButton-label'][4]")).click();
//				//List<WebElement> ele = web_driver.findElements(By.xpath("//descendant::div[@class='sc-nFpLZ jXWXLS']"));
//				basePageWeb.waitforPageLoad(5);
//				
//			}
			
			
//			WebElement elements = ele.get(i);
//			if(elements.equals("Darling Rose Gold"))
//			{
//				
//			}
			
		}
		
		
		
	
	

		
		
		
}
