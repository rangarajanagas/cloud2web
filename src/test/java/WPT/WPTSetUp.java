package WPT;

import java.io.IOException;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import Helper.Efficacies;
import Helper.ExtentTestManager;
import Helper.StartApp;
import WebPages.WPTBasePage;
import WebPages.WPTSanityPage;

public class WPTSetUp {

	public Efficacies eff;
	public WPTSanityPage sanityPageWeb;
	public WPTBasePage basePageWeb;
	public WebDriver web_driver;
	public StartApp startApp;
	public MyLibraryTest hometest;
	
	@BeforeSuite
	public void loginSetUp() throws Exception
	{
		startApp = new StartApp();
		eff = new Efficacies();
		web_driver = startApp.startApp_Web();
		sanityPageWeb = new WPTSanityPage(web_driver);
		basePageWeb = new WPTBasePage(web_driver);
		hometest = new MyLibraryTest();

		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "baseData");
		basePageWeb.enterText(sanityPageWeb.txtusername, testData.get("username"));
		basePageWeb.enterText(sanityPageWeb.txtpassword, testData.get("password"));
		basePageWeb.clickElement(sanityPageWeb.btnLogin);
		basePageWeb.waitforPageLoad(2);
	}
	
	
	@BeforeClass(alwaysRun = true)
	public void setUp() throws Exception {
		
		
		
	}
	

	//@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
//		ExtentTestManager.getTest().info("Close the web driver...");
		startApp.closeDriver_Web();
	}

}
