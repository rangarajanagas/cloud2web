package WPT;

import java.util.ArrayList;
import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MyLibraryTest extends WPTSetUp {
	
	@Test(priority=1,groups = "Cloud2_WPT", description = "Validate Authentication by entering Valid Credentials")
	public void verify_LoginPopup() throws Exception {

		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_LoginPopup");
		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(5);
		web_driver.findElement(sanityPageWeb.chkbxLegal).click();					
		basePageWeb.clickElement(sanityPageWeb.btnContinue);		
		actualData.add(basePageWeb.getElement(sanityPageWeb.lnkMyLibrary).getText());
		expectedData.add(testData.get("TextMyLibrary"));
		
		Assert.assertEquals(expectedData, actualData);
	}
	
	@Test(priority=2,groups = "Cloud2_WPT", description = "Verify by opening the eBook")
	public void verify_MyLibrary() throws Exception {

		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_MyLibrary");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.lnkMyLibrary);
		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.lnkOpen);
		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.btnReadNow);
		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.lnktoc);
		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.chptrSelect);
		basePageWeb.waitforPageLoad(5);
		Actions actions = new Actions(web_driver);
		actions.sendKeys(Keys.ESCAPE).perform();
		
		Assert.assertEquals(expectedData, actualData);
	}
	
	@Test(priority=3, groups = "Cloud2_WPT", description = "Verify by selecting the Chapters")
	public void verify_NavigateChapter() throws Exception {

		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_MyLibrary");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(3);
		basePageWeb.clickElement(sanityPageWeb.lnkNxtChapter);
		basePageWeb.waitforPageLoad(2);
		basePageWeb.clickElement(sanityPageWeb.lnkPreChapter);
		
		Assert.assertEquals(expectedData, actualData);
	}
	
	@Test(priority=4, groups = "Cloud2_WPT", description = "Verify by selecting the Reader Settings options")
	public void verify_ReaderSettings() throws Exception {

		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_MyLibrary");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(1);
		//Click on the Reader Settings Icon
		basePageWeb.clickElement(sanityPageWeb.lnkReaderSettings);
		basePageWeb.waitforPageLoad(1);
		
		basePageWeb.clickElement(sanityPageWeb.lnkCenterAligned);
		basePageWeb.clickElement(sanityPageWeb.lnkLeftAligned,1);
		basePageWeb.clickElement(sanityPageWeb.lnkColorDay,1);
		basePageWeb.clickElement(sanityPageWeb.lnkColrSepia,1);
		basePageWeb.clickElement(sanityPageWeb.lnkColorNight,1);
		basePageWeb.waitforPageLoad(4);
		web_driver.findElement(sanityPageWeb.lnkPageScroll).click();
		web_driver.findElement(sanityPageWeb.lnkPageScroll).click();
		basePageWeb.clickElement(sanityPageWeb.lnkReset,1);		
		
		//Select the Center alignment option
//		Boolean alignment = web_driver.findElement((sanityPageWeb.lnkCenterAligned)).isSelected();
//		
//		if(alignment==true)
//		{
//			basePageWeb.waitforPageLoad(2);
//			basePageWeb.clickElement(sanityPageWeb.lnkLeftAligned);
//		}
		
		//basePageWeb.clickElement(sanityPageWeb.lnkLeftAligned);
		basePageWeb.waitforPageLoad(1);
		//Click on the Reader Settings Icon
		basePageWeb.clickElement(sanityPageWeb.lnkReaderSettings);
	
		Assert.assertEquals(expectedData, actualData);
	}
	
	@Test(priority=5, groups = "Cloud2_WPT", description = "Verify by adding the Bookmarks")
	public void verify_AddBookmark() throws Exception {

		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_AddBookmark");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.lnkAddBkmark);
		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.lnkBkMarkNotations);
		
		basePageWeb.waitforPageLoad(3);
		actualData.add(basePageWeb.getElement(sanityPageWeb.svdBkmark).getText());		
		expectedData.add(testData.get("TextSavedBookmark"));
		
		Assert.assertEquals(expectedData, actualData);
	}
	
	//@Test(priority=6, groups = "Cloud2_WPT", description = "Verify by deleting the Bookmarks")
	public void verify_DeleteBookmark() throws Exception {

		// ExtentTestManager.getTest().assignCategory("Cloud2_WPT");
		Map<String, String> testData = eff.readJsonElement("SetUp_Web.json", "verify_AddBookmark");
		ArrayList<Object> actualData = new ArrayList<>();
		ArrayList<Object> expectedData = new ArrayList<>();

		basePageWeb.waitforPageLoad(1);
		basePageWeb.clickElement(sanityPageWeb.dltBkmark);
		
		Assert.assertEquals(expectedData, actualData);
	}
	
	
}
